/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./server/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./server/_helpers/db.js":
/*!*******************************!*\
  !*** ./server/_helpers/db.js ***!
  \*******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var mysql2_promise__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! mysql2/promise */ \"mysql2/promise\");\n/* harmony import */ var mysql2_promise__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(mysql2_promise__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var sequelize__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! sequelize */ \"sequelize\");\n/* harmony import */ var sequelize__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(sequelize__WEBPACK_IMPORTED_MODULE_1__);\n/* harmony import */ var _models_urls_url_model__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../models/urls/url.model */ \"./server/models/urls/url.model.js\");\n/* harmony import */ var dotenv__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! dotenv */ \"dotenv\");\n/* harmony import */ var dotenv__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(dotenv__WEBPACK_IMPORTED_MODULE_3__);\n\n\n\n\ndotenv__WEBPACK_IMPORTED_MODULE_3___default.a.config();\nvar db = {};\ninitialize();\n\nasync function initialize() {\n  const {\n    MYSQL_HOST,\n    MYSQL_PORT,\n    MYSQL_USER,\n    MYSQL_PASSWORD,\n    MYSQL_DATABASE\n  } = process.env;\n  const connection = await mysql2_promise__WEBPACK_IMPORTED_MODULE_0___default.a.createConnection({\n    host: MYSQL_HOST,\n    port: MYSQL_PORT,\n    user: MYSQL_USER,\n    password: ''\n  });\n  await connection.query(`CREATE DATABASE IF NOT EXISTS \\`${MYSQL_DATABASE}\\`;`);\n  const sequelize = new sequelize__WEBPACK_IMPORTED_MODULE_1__[\"Sequelize\"](MYSQL_DATABASE, MYSQL_USER, '', {\n    dialect: 'mysql'\n  });\n  db.Url = Object(_models_urls_url_model__WEBPACK_IMPORTED_MODULE_2__[\"default\"])(sequelize);\n  await sequelize.sync();\n}\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (db);\n\n//# sourceURL=webpack:///./server/_helpers/db.js?");

/***/ }),

/***/ "./server/_middleware/validate-request.js":
/*!************************************************!*\
  !*** ./server/_middleware/validate-request.js ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"default\", function() { return validateRequest; });\nfunction validateRequest(req, next, schema) {\n  const options = {\n    abortEarly: false,\n    // include all errors\n    allowUnknown: true,\n    // ignore unknown props\n    stripUnknown: true // remove unknown props\n\n  };\n  const {\n    error,\n    value\n  } = schema.validate(req.body, options);\n\n  if (error) {\n    next(`Validation error: ${error.details.map(x => x.message).join(', ')}`);\n  } else {\n    req.body = value;\n    next();\n  }\n}\n\n//# sourceURL=webpack:///./server/_middleware/validate-request.js?");

/***/ }),

/***/ "./server/index.js":
/*!*************************!*\
  !*** ./server/index.js ***!
  \*************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-runtime */ \"react/jsx-runtime\");\n/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var path__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! path */ \"path\");\n/* harmony import */ var path__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(path__WEBPACK_IMPORTED_MODULE_1__);\n/* harmony import */ var fs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! fs */ \"fs\");\n/* harmony import */ var fs__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(fs__WEBPACK_IMPORTED_MODULE_2__);\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react */ \"react\");\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_3__);\n/* harmony import */ var express__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! express */ \"express\");\n/* harmony import */ var express__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(express__WEBPACK_IMPORTED_MODULE_4__);\n/* harmony import */ var react_dom_server__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! react-dom/server */ \"react-dom/server\");\n/* harmony import */ var react_dom_server__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(react_dom_server__WEBPACK_IMPORTED_MODULE_5__);\n/* harmony import */ var _models_urls_url_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./models/urls/url.service */ \"./server/models/urls/url.service.js\");\n/* harmony import */ var _models_urls_urls_controller__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./models/urls/urls.controller */ \"./server/models/urls/urls.controller.js\");\n/* harmony import */ var dotenv__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! dotenv */ \"dotenv\");\n/* harmony import */ var dotenv__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(dotenv__WEBPACK_IMPORTED_MODULE_8__);\n/* harmony import */ var _src_App__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../src/App */ \"./src/App.js\");\n\n\n\n\n\n\n\n\n\ndotenv__WEBPACK_IMPORTED_MODULE_8___default.a.config();\n\nconst PORT = process.env.PORT || 3006;\nconst app = express__WEBPACK_IMPORTED_MODULE_4___default()();\napp.use(express__WEBPACK_IMPORTED_MODULE_4___default.a.json());\napp.use('/api/urls', _models_urls_urls_controller__WEBPACK_IMPORTED_MODULE_7__[\"default\"]);\napp.get('/:token', async (req, res) => {\n  const {\n    token\n  } = req.params;\n\n  if (!token.includes('.')) {\n    const url = await _models_urls_url_service__WEBPACK_IMPORTED_MODULE_6__[\"default\"].getByToken(token);\n    res.writeHead(301, {\n      Location: url.url\n    });\n    res.end();\n  }\n});\napp.get('/', (req, res) => {\n  const app = react_dom_server__WEBPACK_IMPORTED_MODULE_5___default.a.renderToString( /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__[\"jsx\"])(_src_App__WEBPACK_IMPORTED_MODULE_9__[\"default\"], {}));\n  const indexFile = path__WEBPACK_IMPORTED_MODULE_1___default.a.resolve('./build/index.html');\n  fs__WEBPACK_IMPORTED_MODULE_2___default.a.readFile(indexFile, 'utf8', (err, data) => {\n    if (err) {\n      console.error('Something went wrong:', err);\n      return res.status(500).send('Oops, better luck next time!');\n    }\n\n    return res.send(data.replace('<div id=\"root\"></div>', `<div id=\"root\">${app}</div>`));\n  });\n});\napp.use(express__WEBPACK_IMPORTED_MODULE_4___default.a.static('./build'));\napp.listen(PORT, () => {\n  console.log(`Server is listening on port ${PORT}`);\n});\n\n//# sourceURL=webpack:///./server/index.js?");

/***/ }),

/***/ "./server/models/urls/url.model.js":
/*!*****************************************!*\
  !*** ./server/models/urls/url.model.js ***!
  \*****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"default\", function() { return model; });\n/* harmony import */ var sequelize__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! sequelize */ \"sequelize\");\n/* harmony import */ var sequelize__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(sequelize__WEBPACK_IMPORTED_MODULE_0__);\n\nfunction model(sequelize) {\n  const attributes = {\n    id: {\n      type: sequelize__WEBPACK_IMPORTED_MODULE_0__[\"Sequelize\"].INTEGER,\n      autoIncrement: true,\n      primaryKey: true\n    },\n    url: {\n      type: sequelize__WEBPACK_IMPORTED_MODULE_0__[\"Sequelize\"].STRING,\n      allowNull: false\n    },\n    token: {\n      type: 'VARCHAR(10) CHARACTER SET utf8 COLLATE utf8_bin',\n      allowNull: false,\n      unique: true\n    }\n  };\n  const options = {};\n  return sequelize.define('Url', attributes, options);\n}\n\n//# sourceURL=webpack:///./server/models/urls/url.model.js?");

/***/ }),

/***/ "./server/models/urls/url.service.js":
/*!*******************************************!*\
  !*** ./server/models/urls/url.service.js ***!
  \*******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _helpers_db__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../_helpers/db */ \"./server/_helpers/db.js\");\n/* harmony import */ var rand_token__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rand-token */ \"rand-token\");\n/* harmony import */ var rand_token__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(rand_token__WEBPACK_IMPORTED_MODULE_1__);\n\n\nconst urlService = {\n  getAll: async () => {\n    return await _helpers_db__WEBPACK_IMPORTED_MODULE_0__[\"default\"].Url.findAll();\n  },\n  getById: async id => {\n    return await getUrl(id);\n  },\n  getByToken: async token => {\n    const url = await getUrlByToken(token);\n    if (!url) throw 'Url not found';\n    return url;\n  },\n  create: async params => {\n    // validate\n    params.token = await generateToken(); // save url\n\n    return await _helpers_db__WEBPACK_IMPORTED_MODULE_0__[\"default\"].Url.create(params);\n  },\n  _delete: async id => {\n    const url = await getUrl(id);\n    await url.destroy();\n  }\n}; // helper functions\n\nasync function generateToken() {\n  const token = rand_token__WEBPACK_IMPORTED_MODULE_1___default.a.generate(5);\n\n  if (await getUrlByToken(token)) {\n    generateToken();\n  }\n\n  return token;\n}\n\nasync function getUrl(id) {\n  const url = await _helpers_db__WEBPACK_IMPORTED_MODULE_0__[\"default\"].Url.findByPk(id);\n  if (!url) throw 'Url not found';\n  return url;\n}\n\nasync function getUrlByToken(token) {\n  const url = await _helpers_db__WEBPACK_IMPORTED_MODULE_0__[\"default\"].Url.findOne({\n    where: {\n      token\n    }\n  });\n  return url;\n}\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (urlService);\n\n//# sourceURL=webpack:///./server/models/urls/url.service.js?");

/***/ }),

/***/ "./server/models/urls/urls.controller.js":
/*!***********************************************!*\
  !*** ./server/models/urls/urls.controller.js ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _middleware_validate_request__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../_middleware/validate-request */ \"./server/_middleware/validate-request.js\");\n/* harmony import */ var _url_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./url.service */ \"./server/models/urls/url.service.js\");\nconst express = __webpack_require__(/*! express */ \"express\");\n\nconst router = express.Router();\n\nconst Joi = __webpack_require__(/*! joi */ \"joi\");\n\n\n // routes\n\nrouter.post('/', createSchema, create);\nrouter.get('/', getAll);\nrouter.get('/current', getCurrent);\nrouter.get('/:id', getById);\nrouter.delete('/:id', _delete);\n\nfunction createSchema(req, res, next) {\n  console.log(req.body);\n  const schema = Joi.object({\n    url: Joi.string().required()\n  });\n  Object(_middleware_validate_request__WEBPACK_IMPORTED_MODULE_0__[\"default\"])(req, next, schema);\n}\n\nfunction create(req, res, next) {\n  console.log('req.body', req.body);\n  _url_service__WEBPACK_IMPORTED_MODULE_1__[\"default\"].create(req.body).then(url => res.json(url)).catch(next);\n}\n\nfunction getAll(req, res, next) {\n  _url_service__WEBPACK_IMPORTED_MODULE_1__[\"default\"].getAll().then(users => res.json(users)).catch(next);\n}\n\nfunction getCurrent(req, res, next) {\n  res.json(req.user);\n}\n\nfunction getById(req, res, next) {\n  _url_service__WEBPACK_IMPORTED_MODULE_1__[\"default\"].getById(req.params.id).then(user => res.json(user)).catch(next);\n}\n\nfunction _delete(req, res, next) {\n  _url_service__WEBPACK_IMPORTED_MODULE_1__[\"default\"].delete(req.params.id).then(() => res.json({\n    message: 'User deleted successfully'\n  })).catch(next);\n}\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (router);\n\n//# sourceURL=webpack:///./server/models/urls/urls.controller.js?");

/***/ }),

/***/ "./src/App.js":
/*!********************!*\
  !*** ./src/App.js ***!
  \********************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"default\", function() { return MyApp; });\n/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-runtime */ \"react/jsx-runtime\");\n/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _components_Shortener_Index__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./components/Shortener/Index */ \"./src/components/Shortener/Index.js\");\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react */ \"react\");\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_2__);\n/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! prop-types */ \"prop-types\");\n/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_3__);\n/* harmony import */ var react_toastify__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! react-toastify */ \"react-toastify\");\n/* harmony import */ var react_toastify__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(react_toastify__WEBPACK_IMPORTED_MODULE_4__);\n/* harmony import */ var _src_context_UrlContext__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../src/context/UrlContext */ \"./src/context/UrlContext.js\");\n\n\n// import logo from './logo.svg';\n// import './App.scss';\n // function App() {\n//   return (\n//     <div className=\"App\">\n//       <Shortener />\n//     </div>\n//   );\n// }\n// export default App;\n\n\n\n // import 'react-toastify/dist/ReactToastify.css';\n\n // import '../styles/index.scss'\n\nfunction MyApp() {\n  Object(react__WEBPACK_IMPORTED_MODULE_2__[\"useEffect\"])(() => {\n    // Remove the server-side injected CSS.\n    const jssStyles = document.querySelector('#jss-server-side');\n\n    if (jssStyles) {\n      jssStyles.parentElement.removeChild(jssStyles);\n    }\n  }, []);\n  return (\n    /*#__PURE__*/\n    // <MainLayout>\n    Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__[\"jsxs\"])(_src_context_UrlContext__WEBPACK_IMPORTED_MODULE_5__[\"Provider\"], {\n      children: [/*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__[\"jsx\"])(_components_Shortener_Index__WEBPACK_IMPORTED_MODULE_1__[\"default\"], {}), /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__[\"jsx\"])(react_toastify__WEBPACK_IMPORTED_MODULE_4__[\"ToastContainer\"], {\n        position: \"bottom-right\"\n      })]\n    }) // </MainLayout>\n\n  );\n}\nMyApp.propTypes = {};\n\n//# sourceURL=webpack:///./src/App.js?");

/***/ }),

/***/ "./src/api/jsonServer.js":
/*!*******************************!*\
  !*** ./src/api/jsonServer.js ***!
  \*******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! axios */ \"axios\");\n/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_0__);\n\nconst instance = axios__WEBPACK_IMPORTED_MODULE_0___default.a.create({\n  baseURL: `http://localhost:3006/api`\n});\n/* harmony default export */ __webpack_exports__[\"default\"] = (instance);\n\n//# sourceURL=webpack:///./src/api/jsonServer.js?");

/***/ }),

/***/ "./src/components/Button.js":
/*!**********************************!*\
  !*** ./src/components/Button.js ***!
  \**********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-runtime */ \"react/jsx-runtime\");\n/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! prop-types */ \"prop-types\");\n/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_1__);\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react */ \"react\");\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_2__);\n\n\n\n\nconst Button = ({\n  color,\n  variant,\n  fullWidth,\n  children,\n  style,\n  ...rest\n}) => {\n  const [textColor, setTextColor] = Object(react__WEBPACK_IMPORTED_MODULE_2__[\"useState\"])('#FFF');\n  const [backgroundColor, setBackgroundColor] = Object(react__WEBPACK_IMPORTED_MODULE_2__[\"useState\"])('#CCC');\n  const [borderColor, setBorderColor] = Object(react__WEBPACK_IMPORTED_MODULE_2__[\"useState\"])('#000');\n  const [width, setWidth] = Object(react__WEBPACK_IMPORTED_MODULE_2__[\"useState\"])('100px');\n  Object(react__WEBPACK_IMPORTED_MODULE_2__[\"useEffect\"])(() => {\n    switch (color) {\n      case 'primary':\n        setBackgroundColor('#8da2c1');\n        setBorderColor('#8da2c1');\n        setTextColor('#FFFFFF');\n        break;\n\n      default:\n        setBackgroundColor('#FFF');\n        setTextColor('#CCC');\n        setBorderColor('#CCC');\n        break;\n    }\n\n    switch (variant) {\n      case 'outlined':\n        setBackgroundColor('transparent');\n        setTextColor('#CCCCCC');\n        break;\n\n      default:\n        break;\n    }\n\n    if (fullWidth) {\n      setWidth('100%');\n    } else {\n      setWidth('inherit');\n    }\n  }, []);\n  const buttonStyles = {\n    backgroundColor,\n    color: textColor,\n    padding: '10px 30px',\n    borderRadius: '35px',\n    borderColor,\n    width,\n    borderStyle: 'solid',\n    borderWidth: '1px',\n    fontSize: 22,\n    fontWeight: 300\n  };\n  return /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__[\"jsx\"])(\"button\", {\n    color: color,\n    style: { ...buttonStyles,\n      ...style\n    },\n    ...rest,\n    children: children\n  });\n};\n\nButton.propTypes = {};\nButton.defaultProps = {};\n/* harmony default export */ __webpack_exports__[\"default\"] = (Button);\n\n//# sourceURL=webpack:///./src/components/Button.js?");

/***/ }),

/***/ "./src/components/Shortener/Form.js":
/*!******************************************!*\
  !*** ./src/components/Shortener/Form.js ***!
  \******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-runtime */ \"react/jsx-runtime\");\n/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ \"react\");\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);\n/* harmony import */ var yup__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! yup */ \"yup\");\n/* harmony import */ var yup__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(yup__WEBPACK_IMPORTED_MODULE_2__);\n/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! prop-types */ \"prop-types\");\n/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_3__);\n/* harmony import */ var formik__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! formik */ \"formik\");\n/* harmony import */ var formik__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(formik__WEBPACK_IMPORTED_MODULE_4__);\n/* harmony import */ var _material_ui_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @material-ui/core */ \"@material-ui/core\");\n/* harmony import */ var _material_ui_core__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_material_ui_core__WEBPACK_IMPORTED_MODULE_5__);\n/* harmony import */ var _Button__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../Button */ \"./src/components/Button.js\");\n/* harmony import */ var _context_UrlContext__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../context/UrlContext */ \"./src/context/UrlContext.js\");\n\n\n\n\n\n\n\n\n // const useStyles = makeStyles(() => ({\n//   root: {}\n// }))\n\nfunction ShortenerForm({\n  onSubmitSuccess,\n  ...rest\n}) {\n  // const classes = useStyles()\n  const {\n    urlState,\n    shortIt\n  } = Object(react__WEBPACK_IMPORTED_MODULE_1__[\"useContext\"])(_context_UrlContext__WEBPACK_IMPORTED_MODULE_7__[\"Context\"]); // useEffect(() => {\n  //   if (urlState.error) {\n  //     toast.error(urlState.error.message)\n  //   }\n  // }, [urlState.error])\n\n  return /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__[\"jsx\"])(formik__WEBPACK_IMPORTED_MODULE_4__[\"Formik\"], {\n    initialValues: {\n      url: ''\n    },\n    validationSchema: yup__WEBPACK_IMPORTED_MODULE_2__[\"object\"]().shape({\n      url: yup__WEBPACK_IMPORTED_MODULE_2__[\"string\"]().url('Must be a valid url').max(500).required('Url is required')\n    }),\n    onSubmit: async (values, {\n      setErrors,\n      setStatus,\n      setSubmitting\n    }) => {\n      shortIt({\n        url: values.url\n      }, callback => {\n        onSubmitSuccess(callback.token);\n      });\n    },\n    children: ({\n      errors,\n      handleBlur,\n      handleChange,\n      handleSubmit,\n      isSubmitting,\n      touched,\n      values\n    }) => /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__[\"jsx\"])(\"form\", {\n      noValidate: true,\n      onSubmit: handleSubmit,\n      ...rest,\n      children: /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__[\"jsxs\"])(_material_ui_core__WEBPACK_IMPORTED_MODULE_5__[\"Grid\"], {\n        container: true,\n        spacing: 2,\n        children: [/*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__[\"jsx\"])(_material_ui_core__WEBPACK_IMPORTED_MODULE_5__[\"Grid\"], {\n          item: true,\n          xs: 12,\n          children: /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__[\"jsx\"])(_material_ui_core__WEBPACK_IMPORTED_MODULE_5__[\"TextField\"], {\n            error: Boolean(touched.url && errors.url),\n            fullWidth: true,\n            autoFocus: true,\n            helperText: touched.url && errors.url,\n            label: \"Url\",\n            name: \"url\",\n            onBlur: handleBlur,\n            onChange: handleChange,\n            type: \"url\",\n            value: values.url,\n            variant: \"outlined\"\n          })\n        }), /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__[\"jsxs\"])(_material_ui_core__WEBPACK_IMPORTED_MODULE_5__[\"Grid\"], {\n          item: true,\n          xs: 12,\n          children: [/*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__[\"jsx\"])(_Button__WEBPACK_IMPORTED_MODULE_6__[\"default\"], {\n            color: \"primary\",\n            disabled: isSubmitting,\n            fullWidth: true,\n            type: \"submit\",\n            variant: \"contained\",\n            children: \"Short it\"\n          }), errors.submit && /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__[\"jsx\"])(_material_ui_core__WEBPACK_IMPORTED_MODULE_5__[\"Box\"], {\n            mt: 3,\n            children: /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__[\"jsx\"])(_material_ui_core__WEBPACK_IMPORTED_MODULE_5__[\"FormHelperText\"], {\n              error: true,\n              children: errors.submit\n            })\n          })]\n        })]\n      })\n    })\n  });\n}\n\nShortenerForm.propTypes = {\n  onSubmitSuccess: prop_types__WEBPACK_IMPORTED_MODULE_3___default.a.func\n};\nShortenerForm.defaultProps = {\n  onSubmitSuccess: () => {}\n};\n/* harmony default export */ __webpack_exports__[\"default\"] = (ShortenerForm);\n\n//# sourceURL=webpack:///./src/components/Shortener/Form.js?");

/***/ }),

/***/ "./src/components/Shortener/Index.js":
/*!*******************************************!*\
  !*** ./src/components/Shortener/Index.js ***!
  \*******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-runtime */ \"react/jsx-runtime\");\n/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ \"react\");\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);\n/* harmony import */ var _material_ui_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @material-ui/core */ \"@material-ui/core\");\n/* harmony import */ var _material_ui_core__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_material_ui_core__WEBPACK_IMPORTED_MODULE_2__);\n/* harmony import */ var _Form__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./Form */ \"./src/components/Shortener/Form.js\");\n/* harmony import */ var _Result__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./Result */ \"./src/components/Shortener/Result.js\");\n\n\n\n\n // import './shortener.scss'\n\nfunction ShortenerIndex({}) {\n  const [token, setToken] = Object(react__WEBPACK_IMPORTED_MODULE_1__[\"useState\"])();\n\n  const handleOnSubmitSuccess = result => {\n    setToken(result);\n  };\n\n  return /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__[\"jsx\"])(_material_ui_core__WEBPACK_IMPORTED_MODULE_2__[\"Container\"], {\n    maxWidth: \"md\",\n    style: {\n      marginTop: 200\n    },\n    children: /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__[\"jsx\"])(_material_ui_core__WEBPACK_IMPORTED_MODULE_2__[\"Grid\"], {\n      container: true,\n      spacing: 3,\n      children: /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__[\"jsx\"])(_material_ui_core__WEBPACK_IMPORTED_MODULE_2__[\"Grid\"], {\n        item: true,\n        xs: 12,\n        md: 12,\n        children: token ? /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__[\"jsx\"])(_Result__WEBPACK_IMPORTED_MODULE_4__[\"default\"], {\n          token: token,\n          onShortNew: () => setToken(null)\n        }) : /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__[\"jsx\"])(_Form__WEBPACK_IMPORTED_MODULE_3__[\"default\"], {\n          onSubmitSuccess: handleOnSubmitSuccess\n        })\n      })\n    })\n  });\n}\n\nShortenerIndex.propTypes = {};\nShortenerIndex.defaultProps = {};\n/* harmony default export */ __webpack_exports__[\"default\"] = (ShortenerIndex);\n\n//# sourceURL=webpack:///./src/components/Shortener/Index.js?");

/***/ }),

/***/ "./src/components/Shortener/Result.js":
/*!********************************************!*\
  !*** ./src/components/Shortener/Result.js ***!
  \********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-runtime */ \"react/jsx-runtime\");\n/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ \"react\");\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);\n/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! prop-types */ \"prop-types\");\n/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_2__);\n/* harmony import */ var _material_ui_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @material-ui/core */ \"@material-ui/core\");\n/* harmony import */ var _material_ui_core__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_material_ui_core__WEBPACK_IMPORTED_MODULE_3__);\n/* harmony import */ var react_copy_to_clipboard__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! react-copy-to-clipboard */ \"react-copy-to-clipboard\");\n/* harmony import */ var react_copy_to_clipboard__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(react_copy_to_clipboard__WEBPACK_IMPORTED_MODULE_4__);\n/* harmony import */ var react_toastify__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! react-toastify */ \"react-toastify\");\n/* harmony import */ var react_toastify__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(react_toastify__WEBPACK_IMPORTED_MODULE_5__);\n/* harmony import */ var _fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @fortawesome/react-fontawesome */ \"@fortawesome/react-fontawesome\");\n/* harmony import */ var _fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_6__);\n/* harmony import */ var _fortawesome_free_regular_svg_icons__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @fortawesome/free-regular-svg-icons */ \"@fortawesome/free-regular-svg-icons\");\n/* harmony import */ var _fortawesome_free_regular_svg_icons__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(_fortawesome_free_regular_svg_icons__WEBPACK_IMPORTED_MODULE_7__);\n\n\n\n\n\n\n\n\n\nconst useStyles = Object(_material_ui_core__WEBPACK_IMPORTED_MODULE_3__[\"makeStyles\"])({\n  topText: {\n    fontSize: 20,\n    marginBottom: 20\n  },\n  resultBoxContainer: {\n    border: '1px solid #CCC',\n    borderRadius: 40,\n    padding: '10px 25px',\n    fontSize: 23,\n    position: 'relative'\n  },\n  iconsContainer: {\n    position: 'absolute',\n    right: 25,\n    top: 10\n  },\n  bottomText: {\n    marginTop: 20,\n    fontSize: 14\n  },\n  icon: {\n    color: '#8da2c1'\n  }\n});\n\nfunction ShortenerResult({\n  token,\n  onShortNew\n}) {\n  const classes = useStyles();\n  Object(react__WEBPACK_IMPORTED_MODULE_1__[\"useEffect\"])(() => {}, []);\n  return /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__[\"jsxs\"])(\"div\", {\n    className: \"result-container\",\n    children: [/*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__[\"jsx\"])(\"div\", {\n      className: classes.topText,\n      children: \"This is your shortened url, you can share it anywhere:\"\n    }), /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__[\"jsxs\"])(\"div\", {\n      className: classes.resultBoxContainer,\n      children: [`http://localhost:3006/${token}`, /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__[\"jsxs\"])(\"div\", {\n        className: classes.iconsContainer,\n        children: [/*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__[\"jsx\"])(_material_ui_core__WEBPACK_IMPORTED_MODULE_3__[\"Tooltip\"], {\n          title: \"Short a new url\",\n          placement: \"top\",\n          children: /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__[\"jsx\"])(\"span\", {\n            onClick: onShortNew,\n            style: {\n              marginRight: 12,\n              cursor: 'pointer'\n            },\n            children: /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__[\"jsx\"])(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_6__[\"FontAwesomeIcon\"], {\n              icon: _fortawesome_free_regular_svg_icons__WEBPACK_IMPORTED_MODULE_7__[\"faPaperPlane\"],\n              className: classes.icon\n            })\n          })\n        }), /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__[\"jsx\"])(react_copy_to_clipboard__WEBPACK_IMPORTED_MODULE_4__[\"CopyToClipboard\"], {\n          text: `http://localhost:3006/${token}`,\n          onCopy: () => react_toastify__WEBPACK_IMPORTED_MODULE_5__[\"toast\"].success('Successfully copied to clipboard.'),\n          children: /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__[\"jsx\"])(_material_ui_core__WEBPACK_IMPORTED_MODULE_3__[\"Tooltip\"], {\n            title: \"Copy to clipboard\",\n            placement: \"top\",\n            children: /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__[\"jsx\"])(\"span\", {\n              children: /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__[\"jsx\"])(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_6__[\"FontAwesomeIcon\"], {\n                icon: _fortawesome_free_regular_svg_icons__WEBPACK_IMPORTED_MODULE_7__[\"faCopy\"],\n                className: classes.icon\n              })\n            })\n          })\n        })]\n      })]\n    }), /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__[\"jsx\"])(\"div\", {\n      className: classes.bottomText,\n      children: \"Ad culpa proident irure cupidatat sint officia eu amet. Officia id ut fugiat voluptate. Ex aliqua quis minim sit ullamco Lorem amet eiusmod anim quis laboris quis. Nisi esse enim nostrud pariatur aute in ad. Nisi dolor sit commodo dolor do. Et nulla tempor sint deserunt ad cupidatat duis minim sunt ad dolor.\"\n    })]\n  });\n}\n\nShortenerResult.propTypes = {\n  onSubmitSuccess: prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.string\n};\nShortenerResult.defaultProps = {};\n/* harmony default export */ __webpack_exports__[\"default\"] = (ShortenerResult);\n\n//# sourceURL=webpack:///./src/components/Shortener/Result.js?");

/***/ }),

/***/ "./src/context/UrlContext.js":
/*!***********************************!*\
  !*** ./src/context/UrlContext.js ***!
  \***********************************/
/*! exports provided: Context, Provider */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"Context\", function() { return Context; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"Provider\", function() { return Provider; });\n/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-runtime */ \"react/jsx-runtime\");\n/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ \"react\");\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);\n/* harmony import */ var _api_jsonServer__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../api/jsonServer */ \"./src/api/jsonServer.js\");\n\n\n // import errorHandling from '../api/errorHandling'\n\nconst createDataContext = (reducer, actions, initialState) => {\n  const Context = /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createContext();\n\n  const Provider = ({\n    children\n  }) => {\n    const [urlState, dispatch] = Object(react__WEBPACK_IMPORTED_MODULE_1__[\"useReducer\"])(reducer, initialState);\n    const boundActions = {};\n\n    for (let key in actions) {\n      boundActions[key] = actions[key](dispatch);\n    }\n\n    return /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__[\"jsx\"])(Context.Provider, {\n      value: {\n        urlState,\n        ...boundActions\n      },\n      children: children\n    });\n  };\n\n  return {\n    Context,\n    Provider\n  };\n};\n\nconst urlReducer = (state, action) => {\n  switch (action.type) {\n    case 'SHORTIT_LOADING':\n      return { ...state,\n        url: null,\n        loading: action.payload\n      };\n\n    case 'SHORTIT_SUCCESS':\n      return { ...state,\n        token: action.payload,\n        loading: false\n      };\n\n    case 'SHORTIT_ERROR':\n      return { ...state,\n        loading: false,\n        error: action.payload\n      };\n\n    case 'GET_SUCCESS':\n      return { ...state,\n        url: action.payload,\n        loading: false\n      };\n\n    default:\n      return state;\n  }\n};\n\nconst shortIt = dispatch => async (url, callback) => {\n  try {\n    dispatch({\n      type: 'SHORTIT_LOADING',\n      payload: true\n    });\n    const response = await _api_jsonServer__WEBPACK_IMPORTED_MODULE_2__[\"default\"].post(`/urls`, url);\n    dispatch({\n      type: 'SHORTIT_SUCCESS',\n      payload: response.data\n    });\n\n    if (callback) {\n      callback(response.data);\n    }\n  } catch (error) {\n    console.log('SHORTIT_ERROR -> error', error);\n    dispatch({\n      type: 'SHORTIT_ERROR',\n      payload: error.response.data.error //errorResponse(error.response),\n\n    });\n  }\n};\n\nconst getUrls = dispatch => async urlId => {\n  try {\n    dispatch({\n      type: 'LIST_USERS_LOADING',\n      payload: true\n    });\n    const response = await _api_jsonServer__WEBPACK_IMPORTED_MODULE_2__[\"default\"].get(`/urls`);\n    console.log('getUrls -> getUrls', response.data);\n    dispatch({\n      type: 'LIST_USERS_SUCCESS',\n      payload: response.data\n    });\n  } catch (error) {\n    dispatch({\n      type: 'LIST_USERS_ERROR',\n      payload: error.response\n    });\n  }\n};\n\nconst {\n  Context,\n  Provider\n} = createDataContext(urlReducer, {\n  shortIt\n}, {\n  token: {},\n  loading: false\n});\n\n//# sourceURL=webpack:///./src/context/UrlContext.js?");

/***/ }),

/***/ "@fortawesome/free-regular-svg-icons":
/*!******************************************************!*\
  !*** external "@fortawesome/free-regular-svg-icons" ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"@fortawesome/free-regular-svg-icons\");\n\n//# sourceURL=webpack:///external_%22@fortawesome/free-regular-svg-icons%22?");

/***/ }),

/***/ "@fortawesome/react-fontawesome":
/*!*************************************************!*\
  !*** external "@fortawesome/react-fontawesome" ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"@fortawesome/react-fontawesome\");\n\n//# sourceURL=webpack:///external_%22@fortawesome/react-fontawesome%22?");

/***/ }),

/***/ "@material-ui/core":
/*!************************************!*\
  !*** external "@material-ui/core" ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"@material-ui/core\");\n\n//# sourceURL=webpack:///external_%22@material-ui/core%22?");

/***/ }),

/***/ "axios":
/*!************************!*\
  !*** external "axios" ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"axios\");\n\n//# sourceURL=webpack:///external_%22axios%22?");

/***/ }),

/***/ "dotenv":
/*!*************************!*\
  !*** external "dotenv" ***!
  \*************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"dotenv\");\n\n//# sourceURL=webpack:///external_%22dotenv%22?");

/***/ }),

/***/ "express":
/*!**************************!*\
  !*** external "express" ***!
  \**************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"express\");\n\n//# sourceURL=webpack:///external_%22express%22?");

/***/ }),

/***/ "formik":
/*!*************************!*\
  !*** external "formik" ***!
  \*************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"formik\");\n\n//# sourceURL=webpack:///external_%22formik%22?");

/***/ }),

/***/ "fs":
/*!*********************!*\
  !*** external "fs" ***!
  \*********************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"fs\");\n\n//# sourceURL=webpack:///external_%22fs%22?");

/***/ }),

/***/ "joi":
/*!**********************!*\
  !*** external "joi" ***!
  \**********************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"joi\");\n\n//# sourceURL=webpack:///external_%22joi%22?");

/***/ }),

/***/ "mysql2/promise":
/*!*********************************!*\
  !*** external "mysql2/promise" ***!
  \*********************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"mysql2/promise\");\n\n//# sourceURL=webpack:///external_%22mysql2/promise%22?");

/***/ }),

/***/ "path":
/*!***********************!*\
  !*** external "path" ***!
  \***********************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"path\");\n\n//# sourceURL=webpack:///external_%22path%22?");

/***/ }),

/***/ "prop-types":
/*!*****************************!*\
  !*** external "prop-types" ***!
  \*****************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"prop-types\");\n\n//# sourceURL=webpack:///external_%22prop-types%22?");

/***/ }),

/***/ "rand-token":
/*!*****************************!*\
  !*** external "rand-token" ***!
  \*****************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"rand-token\");\n\n//# sourceURL=webpack:///external_%22rand-token%22?");

/***/ }),

/***/ "react":
/*!************************!*\
  !*** external "react" ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"react\");\n\n//# sourceURL=webpack:///external_%22react%22?");

/***/ }),

/***/ "react-copy-to-clipboard":
/*!******************************************!*\
  !*** external "react-copy-to-clipboard" ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"react-copy-to-clipboard\");\n\n//# sourceURL=webpack:///external_%22react-copy-to-clipboard%22?");

/***/ }),

/***/ "react-dom/server":
/*!***********************************!*\
  !*** external "react-dom/server" ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"react-dom/server\");\n\n//# sourceURL=webpack:///external_%22react-dom/server%22?");

/***/ }),

/***/ "react-toastify":
/*!*********************************!*\
  !*** external "react-toastify" ***!
  \*********************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"react-toastify\");\n\n//# sourceURL=webpack:///external_%22react-toastify%22?");

/***/ }),

/***/ "react/jsx-runtime":
/*!************************************!*\
  !*** external "react/jsx-runtime" ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"react/jsx-runtime\");\n\n//# sourceURL=webpack:///external_%22react/jsx-runtime%22?");

/***/ }),

/***/ "sequelize":
/*!****************************!*\
  !*** external "sequelize" ***!
  \****************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"sequelize\");\n\n//# sourceURL=webpack:///external_%22sequelize%22?");

/***/ }),

/***/ "yup":
/*!**********************!*\
  !*** external "yup" ***!
  \**********************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"yup\");\n\n//# sourceURL=webpack:///external_%22yup%22?");

/***/ })

/******/ });