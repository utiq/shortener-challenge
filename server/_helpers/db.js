
import mysql from 'mysql2/promise';
import { Sequelize } from 'sequelize';
import Url from '../models/urls/url.model'

import dotenv from 'dotenv';
dotenv.config()

var db = {};

initialize();

async function initialize() {
  const { MYSQL_HOST, MYSQL_PORT, MYSQL_USER, MYSQL_PASSWORD, MYSQL_DATABASE } = process.env

  const connection = await mysql.createConnection({
    host: MYSQL_HOST,
    port: MYSQL_PORT,
    user: MYSQL_USER,
    password: ''
  });
  await connection.query(`CREATE DATABASE IF NOT EXISTS \`${MYSQL_DATABASE}\`;`);

  const sequelize = new Sequelize(MYSQL_DATABASE, MYSQL_USER, '', {
    dialect: 'mysql'
  });

  db.Url = (Url)(sequelize);

  await sequelize.sync();
}

export default db;