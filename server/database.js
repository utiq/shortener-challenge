import Sequelize from 'sequelize'
import mysql from 'mysql2/promise' 

export default async function database() {

  const { MYSQL_HOST, MYSQL_PORT, MYSQL_USER, MYSQL_PASSWORD, MYSQL_DATABASE } = process.env

  const connection = await mysql.createConnection({
    host: MYSQL_HOST,
    port: MYSQL_PORT,
    user: MYSQL_USER,
    password: ''
  });
  await connection.query(`CREATE DATABASE IF NOT EXISTS \`${MYSQL_DATABASE}\`;`);

  const sequelize = new Sequelize(MYSQL_DATABASE, MYSQL_USER, '', {
    dialect: 'mysql'
  });

  const urls = sequelize.define('url', {
    id: {
      type: Sequelize.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    url: {
      type: Sequelize.STRING,
      allowNull: false
    },
    token: {
      type: Sequelize.STRING,
      allowNull: false,
      unique: true
    }
  });

  await sequelize.sync();

  // await users.create({ url: 'http://google.com', token: '98wehA1' })

  return sequelize
}