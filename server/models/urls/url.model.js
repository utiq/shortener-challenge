import { Sequelize } from 'sequelize';

export default function model(sequelize) {
  const attributes = {
    id: {
      type: Sequelize.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    url: {
      type: Sequelize.STRING,
      allowNull: false
    },
    token: {
      type: 'VARCHAR(10) CHARACTER SET utf8 COLLATE utf8_bin',
      allowNull: false,
      unique: true
    }
  };

  const options = {
  };

  return sequelize.define('Url', attributes, options);
}