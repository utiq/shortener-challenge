import db from '../../_helpers/db';
import randtoken from 'rand-token'

const urlService = {
  getAll: async () => {
    return await db.Url.findAll();
  },

  getById: async (id) => {
    return await getUrl(id);
  },

  getByToken: async (token) => {
    const url = await getUrlByToken(token)
    if (!url) throw 'Url not found';
    return url;
  },

  create: async (params) => {
    // validate
    params.token = await generateToken();

    // save url
    return await db.Url.create(params);
  },

  _delete: async (id) => {
    const url = await getUrl(id);
    await url.destroy();
  }
}

// helper functions

async function generateToken() {
  const token = randtoken.generate(5);
  if (await getUrlByToken(token)) {
    generateToken()
  }
  return token
}

async function getUrl(id) {
  const url = await db.Url.findByPk(id);
  if (!url) throw 'Url not found';
  return url;
}

async function getUrlByToken(token) {
  const url = await db.Url.findOne({ where: { token } });
  return url;
}

export default urlService;