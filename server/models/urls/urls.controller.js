const express = require('express');
const router = express.Router();
const Joi = require('joi');
import validateRequest from '../../_middleware/validate-request';
import urlService from './url.service';

// routes
router.post('/', createSchema, create);
router.get('/', getAll);
router.get('/current', getCurrent);
router.get('/:id', getById);
router.delete('/:id', _delete);

function createSchema(req, res, next) {
  console.log(req.body);
  const schema = Joi.object({
    url: Joi.string().required()
  });
  validateRequest(req, next, schema);
}

function create(req, res, next) {
  console.log('req.body', req.body);
  urlService.create(req.body)
    .then((url) => res.json(url))
    .catch(next);
}

function getAll(req, res, next) {
  urlService.getAll()
    .then(users => res.json(users))
    .catch(next);
}

function getCurrent(req, res, next) {
  res.json(req.user);
}

function getById(req, res, next) {
  urlService.getById(req.params.id)
    .then(user => res.json(user))
    .catch(next);
}

function _delete(req, res, next) {
  urlService.delete(req.params.id)
    .then(() => res.json({ message: 'User deleted successfully' }))
    .catch(next);
}

export default router;