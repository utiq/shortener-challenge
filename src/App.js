// import logo from './logo.svg';
// import './App.scss';
import Shortener from './components/Shortener/Index'

// function App() {
//   return (
//     <div className="App">
//       <Shortener />
//     </div>
//   );
// }

// export default App;

import { useEffect } from 'react'
import PropTypes from 'prop-types';
import { ToastContainer, toast } from 'react-toastify';
// import 'react-toastify/dist/ReactToastify.css';
import { Provider as UrlProvider } from '../src/context/UrlContext'

// import '../styles/index.scss'

export default function MyApp() {

  useEffect(() => {
    // Remove the server-side injected CSS.
    const jssStyles = document.querySelector('#jss-server-side');
    if (jssStyles) {
      jssStyles.parentElement.removeChild(jssStyles);
    }
  }, []);

  return (
    // <MainLayout>
      <UrlProvider>
        <Shortener />
        <ToastContainer position="bottom-right" />
      </UrlProvider>
    // </MainLayout>
  );
}

MyApp.propTypes = {
};
