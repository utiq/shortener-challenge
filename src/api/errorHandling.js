// eslint-disable-next-line no-undef
export default errorHandling = (error) => {
  let errorObj = '';
  if (error === undefined || error.config.data === undefined) {
    errorObj = {
      message: 'Error trying to reach out the server, please try again later.',
    };
  } else {
    errorObj = error.data.error;
  }
  return errorObj;
};
