import PropTypes from 'prop-types';
import { useState, useEffect } from 'react';

const Button = ({ color, variant, fullWidth, children, style, ...rest }) => {
  const [textColor, setTextColor] = useState('#FFF')
  const [backgroundColor, setBackgroundColor] = useState('#CCC')
  const [borderColor, setBorderColor] = useState('#000')
  const [width, setWidth] = useState('100px')
  
  useEffect(() => {
    switch (color) {
      case 'primary':
        setBackgroundColor('#8da2c1')
        setBorderColor('#8da2c1')
        setTextColor('#FFFFFF')
        break;
      default:
        setBackgroundColor('#FFF')
        setTextColor('#CCC')
        setBorderColor('#CCC')
        break;
    }

    switch (variant) {
      case 'outlined':
        setBackgroundColor('transparent')
        setTextColor('#CCCCCC')
        break;
      default:
        break;
    }

    if (fullWidth) {
      setWidth('100%')
    } else {
      setWidth('inherit')
    }
  }, []);

  const buttonStyles = {
    backgroundColor,
    color: textColor,
    padding: '10px 30px',
    borderRadius: '35px',
    borderColor,
    width,
    borderStyle: 'solid',
    borderWidth: '1px',
    fontSize: 22,
    fontWeight: 300
  }

  return (
    <button color={color} style={{ ...buttonStyles, ...style }} {...rest}>{children}</button>
	)
}

Button.propTypes = {
}

Button.defaultProps = {
}

export default Button
