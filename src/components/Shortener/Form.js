import React, { useContext, useEffect } from 'react'
import * as Yup from 'yup'
import PropTypes from 'prop-types'
import { Formik } from 'formik'
import {
  Box,
  Grid,
  TextField,
  FormHelperText,
  makeStyles
} from '@material-ui/core'
import Button from '../Button'
import { Context as UrlContext } from '../../context/UrlContext'

// const useStyles = makeStyles(() => ({
//   root: {}
// }))

function ShortenerForm({ onSubmitSuccess, ...rest }) {
  // const classes = useStyles()
  const { urlState, shortIt } = useContext(UrlContext)

  // useEffect(() => {
  //   if (urlState.error) {
  //     toast.error(urlState.error.message)
  //   }
  // }, [urlState.error])

  return (
    <Formik
      initialValues={{
        url: ''
      }}
      validationSchema={Yup.object().shape({
        url: Yup.string()
          .url('Must be a valid url')
          .max(500)
          .required('Url is required')
      })}
      onSubmit={async (values, { setErrors, setStatus, setSubmitting }) => {
        shortIt({ url: values.url }, (callback) => {
          onSubmitSuccess(callback.token)
        })
      }}
    >
      {({
        errors,
        handleBlur,
        handleChange,
        handleSubmit,
        isSubmitting,
        touched,
        values
      }) => (
        <form
          noValidate
          onSubmit={handleSubmit}
          {...rest}
        >
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <TextField
                error={Boolean(touched.url && errors.url)}
                fullWidth
                autoFocus
                helperText={touched.url && errors.url}
                label="Url"
                name="url"
                onBlur={handleBlur}
                onChange={handleChange}
                type="url"
                value={values.url}
                variant="outlined"
              />
            </Grid>
            <Grid item xs={12}>
              <Button
                color="primary"
                disabled={isSubmitting}
                fullWidth
                type="submit"
                variant="contained"
              >
                Short it
              </Button>
              {errors.submit && (
                <Box mt={3}>
                  <FormHelperText error>{errors.submit}</FormHelperText>
                </Box>
              )}
            </Grid>
          </Grid>
        </form>
      )}
    </Formik>
  );
}

ShortenerForm.propTypes = {
  onSubmitSuccess: PropTypes.func
}

ShortenerForm.defaultProps = {
  onSubmitSuccess: () => { }
}

export default ShortenerForm;
