import React, { useState } from 'react'
import {
  Container,
  Grid
} from '@material-ui/core'
import ShortenerForm from './Form'
import ShortenerResult from './Result'

// import './shortener.scss'

function ShortenerIndex({ }) {
  const [token, setToken] = useState()

  const handleOnSubmitSuccess = (result) => {
    setToken(result)
  }

  return (
    <Container maxWidth="md" style={{marginTop: 200}}>
      <Grid container spacing={3}>
        <Grid item xs={12} md={12}>
          { token ? (
            <ShortenerResult token={token} onShortNew={() => setToken(null)}/>
          ): (
            <ShortenerForm onSubmitSuccess={handleOnSubmitSuccess} />
          )}
        </Grid>
      </Grid>
    </Container>
  );
}

ShortenerIndex.propTypes = {
}

ShortenerIndex.defaultProps = {
}

export default ShortenerIndex;
