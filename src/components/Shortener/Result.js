import React, { useContext, useEffect } from 'react'
import PropTypes from 'prop-types'
import {
  Tooltip,
  makeStyles
} from '@material-ui/core'
import { CopyToClipboard } from 'react-copy-to-clipboard'
import { toast } from 'react-toastify'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCopy, faPaperPlane } from '@fortawesome/free-regular-svg-icons'

const useStyles = makeStyles({
  topText: {
    fontSize: 20,
    marginBottom: 20
  },
  resultBoxContainer: {
    border: '1px solid #CCC',
    borderRadius: 40,
    padding: '10px 25px',
    fontSize: 23,
    position: 'relative'
  },
  iconsContainer: {
    position: 'absolute',
    right: 25,
    top: 10
  },
  bottomText: {
    marginTop: 20,
    fontSize: 14
  },
  icon: {
    color: '#8da2c1'
  }
});

function ShortenerResult({ token, onShortNew }) {
  const classes = useStyles();

  useEffect(() => {
  }, [])

  return (
    <div className="result-container">
      <div className={classes.topText}>This is your shortened url, you can share it anywhere:</div>
      <div className={classes.resultBoxContainer}>
        {`http://localhost:3006/${token}`}
        <div className={classes.iconsContainer}>
          <Tooltip title="Short a new url" placement="top">
            <span onClick={onShortNew} style={{marginRight: 12, cursor: 'pointer'}}>
              <FontAwesomeIcon icon={faPaperPlane} className={classes.icon} />
            </span>
          </Tooltip>
          <CopyToClipboard text={`http://localhost:3006/${token}`}
            onCopy={() => toast.success('Successfully copied to clipboard.')}>
            <Tooltip title="Copy to clipboard" placement="top">
              <span><FontAwesomeIcon icon={faCopy} className={classes.icon} /></span>
            </Tooltip>
          </CopyToClipboard>
        </div>
      </div>
      <div className={classes.bottomText}>
        Ad culpa proident irure cupidatat sint officia eu amet. Officia id ut fugiat voluptate. Ex aliqua quis minim sit ullamco Lorem amet eiusmod anim quis laboris quis. Nisi esse enim nostrud pariatur aute in ad. Nisi dolor sit commodo dolor do. Et nulla tempor sint deserunt ad cupidatat duis minim sunt ad dolor.
      </div>
    </div>
  );
}

ShortenerResult.propTypes = {
  onSubmitSuccess: PropTypes.string
}

ShortenerResult.defaultProps = {
}

export default ShortenerResult;
