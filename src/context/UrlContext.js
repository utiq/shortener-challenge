import React, { useReducer } from 'react'
import jsonServer from '../api/jsonServer'
// import errorHandling from '../api/errorHandling'

const createDataContext = (reducer, actions, initialState) => {
  const Context = React.createContext()

  const Provider = ({ children }) => {
    const [urlState, dispatch] = useReducer(reducer, initialState)

    const boundActions = {}
    for (let key in actions) {
      boundActions[key] = actions[key](dispatch)
    }

    return (
      <Context.Provider value={{ urlState, ...boundActions }}>
        {children}
      </Context.Provider>
    )
  }

  return { Context, Provider }
}

const urlReducer = (state, action) => {
  switch (action.type) {
    case 'SHORTIT_LOADING':
      return { ...state, url: null, loading: action.payload }
    case 'SHORTIT_SUCCESS':
      return {
        ...state,
        token: action.payload,
        loading: false
      }
    case 'SHORTIT_ERROR':
      return { ...state, loading: false, error: action.payload }
    case 'GET_SUCCESS':
      return { ...state, url: action.payload, loading: false }
    default:
      return state
  }
}

const shortIt = (dispatch) => async (url, callback) => {
  try {

    dispatch({ type: 'SHORTIT_LOADING', payload: true })
    const response = await jsonServer.post(`/urls`, url)
    dispatch({ type: 'SHORTIT_SUCCESS', payload: response.data })

    if (callback) {
      callback(response.data);
    }

  } catch (error) {
    console.log('SHORTIT_ERROR -> error', error)
    dispatch({
      type: 'SHORTIT_ERROR',
      payload: error.response.data.error, //errorResponse(error.response),
    })
  }
}

const getUrls = (dispatch) => async (urlId) => {
  try {
    dispatch({ type: 'LIST_USERS_LOADING', payload: true })
    const response = await jsonServer.get(`/urls`)
    console.log('getUrls -> getUrls', response.data)
    dispatch({ type: 'LIST_USERS_SUCCESS', payload: response.data })
  } catch (error) {
    dispatch({ type: 'LIST_USERS_ERROR', payload: error.response })
  }

}

export const { Context, Provider } = createDataContext(
  urlReducer,
  {
    shortIt,
  },
  { token: {}, loading: false },
)
